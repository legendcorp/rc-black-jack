/**
 * Deck of Cards
 * Official documentation
 * https://deckofcardsapi.com
 */

const basePath = 'https://deckofcardsapi.com/api/deck';

export default {

    /**
     * Shuffle the Cards
     * Add deck_count as a GET or POST parameter to define the number of Decks you want to use.
     * Blackjack typically uses 6 decks. The default is 1.
     * @param deckCount - Defines the number of Decks you want to use. Deafults to 1
     * @return string - URL Deck of Cards endpoint
     */
    shuffle: ( deckCount = 1 ) => `${basePath}/new/shuffle/?deck_count=${deckCount}`,

    /**
     * Draw a Card
     * The count variable defines how many cards to draw from the deck. Be sure to replace deck_id
     * with a valid deck_id. We use the deck_id as an identifier so we know who is playing with what
     * deck. After two weeks, if no actions have been made on the deck then we throw it away.
     * TIP: replace deckId with "new" to create a shuffled deck and draw cards from that deck in the same request.
     * @param deckId string - Deck identifier
     * @param count number - NUmber of cards to draw
     * @return string - URL Deck of Cards endpoint
     */
    draw: ( deckId: string , count = 1 ) => `${basePath}/${deckId}/draw/?count=${count}`,

    /**
     * Reshuffle the Cards
     * Don't throw away a deck when all you want to do is shuffle. Include the deck_id on your
     * call to shuffle your cards. Don't worry about reminding us how many decks you are playing with.
     * @param deckId string - Deck identifier
     * @return string - URL Deck of Cards endpoint
     */
    reshuffle: ( deckId: string ) => `${basePath}/${deckId}/shuffle/`,

    /**
     * New Deck (arranged in order)
     * Open a brand new deck of cards:
     * A-spades, 2-spades, 3-spades... followed by diamonds, clubs, then hearts.
     * Add jokers_enabled=true as a GET or POST parameter to your request to include two Jokers in the deck.
     * @param jokers boolean - wether to inlcude jokers or not. Defaults to false.
     * @return string - URL Deck of Cards endpoint
     */
    newDeck: ( jokers = false ) => `${basePath}/new/${ jokers ? '?jokers_enabled=true' : '' }`,

    /**
     * Partial Deck
     * If you want to use a partial deck, then you can pass the card codes you want to use using the cards
     * parameter. Separate the card codes with commas, and each card code is a just a two character
     * case-insensitive string:
     * i.e. https://deckofcardsapi.com/api/deck/new/shuffle/?cards=AS,2S,KS,AD,2D,KD,AC,2C,KC,AH,2H,KH
     * The value, one of A (for an ace), 2, 3, 4, 5, 6, 7, 8, 9, 0 (for a ten), J (jack), Q (queen), or K (king);
     * The suit, one of S (Spades), D (Diamonds), C (Clubs), or H (Hearts).
     * In this example, we are asking for a deck consisting of all the aces, twos, and kings.
     * @param cards array - cards to include in new deck
     * @return string - URL Deck of Cards endpoint
     */
    partialDeck: ( cards = [] ) => `${basePath}/new/shuffle/?cards=${ cards.join(',') }`,

    /**
     * Add to Pile
     * Piles can be used for discarding, players hands, or whatever else. Piles are created on the fly,
     * just give a pile a name and add a drawn card to the pile. If the pile didn't exist before, it does now.
     * After a card has been drawn from the deck it can be moved from pile to pile.
     * Note: This will not work with multiple decks.
     * @param deckId string - Deck identifier
     * @param pileName string - Pile identifier
     * @param cards array - cards to include in new deck
     * @return string - URL Deck of Cards endpoint
     */
    addToPile: ( deckId: string, pileName: string, cards = [] ) => `${basePath}/${deckId}/pile/${pileName}/add/?cards=${ cards.join(',') }`,

    /**
     * Shuffle Pile
     * Note: This will not work with multiple decks.
     * @param deckId string - Deck identifier
     * @param pileName string - Pile identifier
     * @return string - URL Deck of Cards endpoint
     */
    shufflePile: ( deckId: string, pileName: string ) => `${basePath}/${deckId}/pile/${pileName}/shuffle/`,

    /**
     * Listing Cards in Piles
     * Note: This will not work with multiple decks.
     * @param deckId string - Deck identifier
     * @param pileName string - Pile identifier
     * @return string - URL Deck of Cards endpoint
     */
    listPile: ( deckId: string, pileName: string ) => `${basePath}/${deckId}/pile/${pileName}/list/`,

    /**
     * Drawing from Piles
     * Specify the cards that you want to draw from the pile. The default is to just draw off the top of the pile
     * (it's a stack). Or add the bottom parameter to the URL to draw from the bottom.
     * @param deckId string - Deck identifier
     * @param pileName string - Pile identifier
     * @return string - URL Deck of Cards endpoint
     */
    drawFromPile: ( deckId: string, pileName: string, count = 1 ) => `${basePath}/${deckId}/pile/${pileName}/draw/?count=${count}`,

};
