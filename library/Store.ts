import { IPersistence, IPersistenceRead } from '@rocket.chat/apps-engine/definition/accessors';
import { RocketChatAssociationModel, RocketChatAssociationRecord } from '@rocket.chat/apps-engine/definition/metadata';
import { IRoom } from '@rocket.chat/apps-engine/definition/rooms';

const blackJackMessage = 'blackJackMessage';

export default {

    /**
     * Updates an existing record by associations with the data provided in the App's persistent storage.
     * Will upsert a new entry if entry existing is not found.
     * @param {Class} persis IPersistence - https://rocketchat.github.io/Rocket.Chat.Apps-ts-definition/interfaces/ipersistence.html#create
     * @param {String} room https://rocketchat.github.io/Rocket.Chat.Apps-ts-definition/interfaces/iroom.html
     * @param {String} deckId Deck identifier
     * @param {Object} data The object to store
     * @returns Promise <string>
     */
    persist: async (persis: IPersistence, room: IRoom, deckId: string, data: object): Promise<string> => {
        const associations: Array<RocketChatAssociationRecord> = [
            new RocketChatAssociationRecord(RocketChatAssociationModel.MISC, blackJackMessage),
            new RocketChatAssociationRecord(RocketChatAssociationModel.ROOM, room.id),
            new RocketChatAssociationRecord(RocketChatAssociationModel.MISC, deckId),
        ];

        try {
            return persis.updateByAssociations(associations, data, true);
        } catch (err) {
            console.warn(err);
            return '';
        }
    },

    /**
     * Updates an existing record by associations with the data provided in the App's persistent storage.
     * Will upsert a new entry if entry existing is not found.
     * @param {Class} persis IPersistence - https://rocketchat.github.io/Rocket.Chat.Apps-ts-definition/interfaces/ipersistence.html#create
     * @param {String} room https://rocketchat.github.io/Rocket.Chat.Apps-ts-definition/interfaces/iroom.html
     * @param {String} deckId Deck identifier
     * @returns Promise <string>
     */
    findByRoom: async (persis: IPersistenceRead, room: IRoom, deckId: string): Promise<any> => {
        const associations: Array<RocketChatAssociationRecord> = [
            new RocketChatAssociationRecord(RocketChatAssociationModel.MISC, blackJackMessage),
            new RocketChatAssociationRecord(RocketChatAssociationModel.ROOM, room.id),
            new RocketChatAssociationRecord(RocketChatAssociationModel.MISC, deckId),
        ];

        try {
            return await persis.readByAssociations(associations);
        } catch (err) {
            console.warn(err);
        }

        return;
    },

};
