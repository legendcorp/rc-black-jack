import {IHttp} from '@rocket.chat/apps-engine/definition/accessors';
import CardsApi from './CardsApi';

export default class Croupier {
    public deckId = '';
    public players = {};
    public winner = '';
    private http: IHttp;
    private playerRoll = [] as any;
    private nextRollPointer = 0;

    constructor(users: Array<string>, http: IHttp) {
        this.registerPlayers(users);
        this.http = http;
        this.playerRoll = users;
    }

    /**
     *
     */
    public setData(data) {
        this.players = data.players;
        this.playerRoll = data.playerRoll;
        this.nextRollPointer = data.nextRollPointer;
        this.deckId = data.deckId;
    }

    /**
     *
     */
    public getData() {
        return {
            players: this.players,
            playerRoll: this.playerRoll,
            nextRollPointer:  this.nextRollPointer,
            deckId: this.deckId,
        };
    }

    /**
     *
     */
    public async getDeck() {
        const shuffleDeckURI = CardsApi.shuffle();
        const shuffleDeckURIResponse = await this.http.get(shuffleDeckURI);
        this.deckId = shuffleDeckURIResponse?.data?.deck_id;
        return this.deckId;
    }

    /**
     *
     */
    public registerPlayers(users: Array<string>) {
        this.players = {};

        users.forEach( (username) => {
            this.players[username] = {};
            this.players[username].cards = [];
            this.players[username].status = 0;
            return;
        } );
    }

    /**
     *
     */
    public async drawInitialCards() {
        let cardsPerPlayer = 2;
        while (cardsPerPlayer--) {
            for (const username in this.players) {
                if (Object.prototype.hasOwnProperty.call(this.players, username)) {
                    await this.drawOneCardForPlayer(username);
                }
            }
        }
    }

    /**
     *
     */
    public async drawOneCardForPlayer(username) {
        const drawCardURI = CardsApi.draw(this.deckId);
        const drawCardURIResponse = await this.http.get(drawCardURI);
        const drawedCard = drawCardURIResponse?.data?.cards;
        this.players[username].cards.push(...drawedCard);
    }

    /**
     *
     */
    public getNextPlayer() {
        const nextPlayerIndex =  this.nextRollPointer;
        this.updateNextRollPointer(nextPlayerIndex);
        console.log( 'this.nextRollPointer', this.nextRollPointer);
        console.log('this.playerRoll', this.playerRoll);
        return this.playerRoll[nextPlayerIndex];

    }

    /**
     *
     */
    public showStatus() {
        console.log( '> showStatus');

        const lastStandingPlayer = this.getLastStandingPlayer();

        this.evaluatePlayerRoll(); // Do all the calculations
        let formattedStatus = '';
        let thisPlayerName;
        let thisPlayerProps;
        let thisPlayerCardsIndex;
        let thisPlayerCards;
        let cardScore;
        let playerStatus;

        console.log( '> this.playerRoll', this.playerRoll );

        for(thisPlayerName in this.players){

            thisPlayerProps = this.players[thisPlayerName];
            formattedStatus += thisPlayerName + ' :arrow_right: ';

            for(thisPlayerCardsIndex in thisPlayerProps['cards']){
                thisPlayerCards = thisPlayerProps['cards'][thisPlayerCardsIndex];
                formattedStatus += this.replaceCardSymbols(thisPlayerCards['code']) + ' ';
            }

            cardScore = this.getPlayerScore(thisPlayerProps);

            console.log('> lastStandingPlayer', lastStandingPlayer );

            if(lastStandingPlayer !== '' && thisPlayerName == lastStandingPlayer){
                playerStatus = 1;
            } else {
                playerStatus = this.observeCardTotals(cardScore);
            }

            switch(playerStatus){
                case 2:
                    formattedStatus += 'BUSTED';
                    break;
                case 1:
                    formattedStatus += 'WINNER';
                    this.winner = thisPlayerName;
                    break;
            }

            formattedStatus += '(' + cardScore + ")\n";

        }

        // console.log( '>> formattedStatus', formattedStatus );

        return formattedStatus;

    }

    /**
     *
     */
    public getLastStandingPlayer() {
        if (this.playerRoll.length === 1) {
            return this.playerRoll.pop();
        }
        return '';
    }

    /**
     *
     */
    public observeCardTotals(cardTotal: number) {

        if (cardTotal === 21) {
            return 1; // Winner
        }
        if (cardTotal > 21) {
            return 2; // Bust
        }

        return 0;
    }

    // PRIVATE METHODS

    /**
     *
     */
    private getPlayerScore(PlayerData: any) {

        if (PlayerData.cards === undefined) {
            return 0;
        }

        const cards = PlayerData.cards;
        let cardTotal = 0;
        let card;
        let thisCardIndex;
        for (thisCardIndex in cards) {
            card = cards[thisCardIndex];
            if (parseInt(card.value) == card.value) {
                cardTotal = cardTotal + parseInt(card.value);
            } else if(card.value.indexOf('A') == 0) {
                cardTotal = cardTotal + 1;
            } else {
                cardTotal = cardTotal + 10;
            }
        }

        if (this.observeCardTotals(cardTotal) > 0) {
            // Its a bust or win
            return cardTotal;
        }

        cardTotal = 0;

        for (thisCardIndex in cards) {
            card = cards[thisCardIndex];
            if (parseInt(card.value) == card.value) {
                cardTotal = cardTotal + parseInt(card.value);
            } else if(card.value.indexOf('A') == 0) {
                cardTotal = cardTotal + 11;
            } else {
                cardTotal = cardTotal + 10;
            }
        }

        return cardTotal;

    }

    /**
     *
     */
    private removePlayerFromPlayerRoll(thisPlayerName: string) {
        console.log( '> removePlayerFromPlayerRoll', thisPlayerName );

        const userIndex = this.playerRoll.indexOf(thisPlayerName);
        console.log( '> userIndex', userIndex, thisPlayerName );
        if ( !this.playerRoll || userIndex === -1 ) { return; }

        this.playerRoll.splice(userIndex, 1);
        console.log( '> b this.playerRoll', this.playerRoll );

        this.nextRollPointer--;

        if (this.nextRollPointer >= this.playerRoll.length) {
            this.nextRollPointer = 0;
        }
        console.log( '> a this.playerRoll', this.playerRoll );
    }

    /**
     *
     */
    private evaluatePlayerRoll() {
        let pokerPlayerProps;
        let playerGameStatus = 0;
        let playerIndex = 0;
        let playerScore = 0;
        let thisPlayerName;

        for (thisPlayerName of this.playerRoll) {
            // thisPlayerName = playerRollList[thisPlayerNameIndex];
            console.log( '> thisPlayerName', thisPlayerName );

            pokerPlayerProps = this.players[thisPlayerName];
            playerScore = this.getPlayerScore(pokerPlayerProps);
            playerGameStatus = this.observeCardTotals(playerScore);
            this.players[thisPlayerName].status = playerGameStatus;

            // Remove the player who's won or busted
            if (playerGameStatus > 0) {
                this.removePlayerFromPlayerRoll(thisPlayerName);
            }

            playerIndex++;
        }
    }

    /**
     *
     */
    private replaceCardSymbols(str: any) {

        str = str.replace('0', '10');

        if (str.indexOf('C') > -1) {
            return str.replace('C', ':clubs:');
        }

        if (str.indexOf('D') > -1) {
            return str.replace('D', ':diamonds:');
        }

        if (str.indexOf('H') > -1) {
            return str.replace('H', ':hearts:');
        }

        if (str.indexOf('S') > -1) {
            return str.replace('S', ':spades:');
        }

    }

    /**
     *
     */
    private updateNextRollPointer(nextPlayerIndex: number) {
        this.nextRollPointer++;

        if ((nextPlayerIndex + 1) >= this.playerRoll.length) {
            this.nextRollPointer = 0;
        }
    }

}
