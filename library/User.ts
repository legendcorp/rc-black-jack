import {IUserRead} from '@rocket.chat/apps-engine/definition/accessors';

/**
 * User class to get and validate users
 */
export default {

    /**
     * Get Users from args
     * @param users
     * @param read
     */
    getValidUsers: async (users: Array<string>, read: IUserRead) => {
        const currUsers: Array<string> = [];
        for (let item of users) {
            item = item.replace('@', '');
            await read.getByUsername(item).then((user) => {
                if (user && user.status === 'online' ) {
                    const userHandle = `@${user.username}`;
                    currUsers.push(userHandle);
                }
                return currUsers;
            } );
        }
        return currUsers;
    },

    /**
     * Validate Users
     * @param currUser
     */
    isUsersValid: (currUser: any) => {
        let valid = true;
        for (const user of currUser) {
            if (user === undefined || user.status !== 'online') {
                valid = false;
            }
        }
        return valid;
    }
}
