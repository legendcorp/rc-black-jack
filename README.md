# Black Jack
Play Black Jack with your colleagues inside Rocket Chat.

## Running a local Rocket Chat instance

To run a local Rocket Chat development instance run:  
``docker run -it --rm -p 3000:3000 -v `pwd`/rocketdb:/var/lib/mongodb rocketchat/rocket.chat.preview``

### __Important Note__
Do not run this command inside the `rc-black-jack` app directory.
It will break the following `rc-apps deploy ...` command as it will create a `/rocketdb` directory with DB info that doesn't needs to be packaged and deployed.

### Following steps
1. Go to http://localhost:3000
2. Create an admin account
3. Enable Apps development mode by clicking your avatar, then navigate to `Administration > General`, then scroll down to `Apps` and click on the radio button over the __Enable development mode__.

## Testing the App
To test your app, having the server running, simply run inside the app project's directory:  
`rc-apps deploy --url http://localhost:3000 --username user_username --password user_password`

Where:

* http://localhost:3000 is your local server URL (if you are running in another port, change the 3000 to the appropriate port)
* `user_username` is the username of your admin user.
* `user_password` is the password of your admin user.

If you want to update the app deployed in your Rocket.Chat instance after making changes to it, you can run:  
`rc-apps deploy --url http://localhost:3000 --username user_username --password user_password --update`

---

## More Useful Information
Now that you have generated a blank default Rocket.Chat App, what are you supposed to do next?
Start developing! Open up your favorite editor, our recommended one is Visual Studio code,
and start working on your App. Once you have something ready to test, you can either
package it up and manually deploy it to your test instance or you can use the CLI to do so.
Here are some commands to get started:
- `rc-apps package`: this command will generate a packaged app file (zip) which can be installed **if** it compiles with TypeScript
- `rc-apps deploy`: this will do what `package` does but will then ask you for your server url, username, and password to deploy it for you

## Documentation

Here are some links to examples and documentation:
- [Rocket.Chat Apps TypeScript Definitions Documentation](https://rocketchat.github.io/Rocket.Chat.Apps-engine/)
- [Rocket.Chat Apps TypeScript Definitions Repository](https://github.com/RocketChat/Rocket.Chat.Apps-engine)
- [Example Rocket.Chat Apps](https://github.com/graywolf336/RocketChatApps)
- Community Forums
  - [App Requests](https://forums.rocket.chat/c/rocket-chat-apps/requests)
  - [App Guides](https://forums.rocket.chat/c/rocket-chat-apps/guides)
  - [Top View of Both Categories](https://forums.rocket.chat/c/rocket-chat-apps)
- [#rocketchat-apps on Open.Rocket.Chat](https://open.rocket.chat/channel/rocketchat-apps)
