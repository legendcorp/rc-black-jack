import {IAppAccessors, ILogger, IModify, IRead} from '@rocket.chat/apps-engine/definition/accessors';
import {IHttp, IPersistence} from '@rocket.chat/apps-engine/definition/accessors';
import {App} from '@rocket.chat/apps-engine/definition/App';
import {IAppInfo} from '@rocket.chat/apps-engine/definition/metadata';
import {ISlashCommand, SlashCommandContext} from '@rocket.chat/apps-engine/definition/slashcommands';

import Croupier from '../library/Croupier';
import Store from '../library/Store';
import User from '../library/User';

export class BlackJackCommand implements ISlashCommand {
    public command = 'blackjack';
    public i18nDescription = 'Starts a Black Jack game.';
    public i18nParamsExample = '@user1 @user2 ... (up to 7 players)';
    public providesPreview = false;
    public storageId: string;

    constructor(private readonly app: App) {}

    public async executor(context: SlashCommandContext, read: IRead, modify: IModify, http: IHttp, persis: IPersistence): Promise<void> {
        const messageStructure = await modify.getCreator().startMessage();
        const contextArgs = context.getArguments(); // the user calling the slashcommand
        const room = context.getRoom(); // the current room

        // Read
        const userReader = read.getUserReader();
        const persistenceReader = read.getPersistenceReader();

        const action = contextArgs[0];

        const methods = {

            /**
             * /blackjack start user1 user2 ...
             */
            start: async (args: Array<string>) => {
                let msg = '';

                // get users
                const users = await User.getValidUsers(args, userReader);

                // get deck
                const blackJack = new Croupier(users, http);
                const deckId = await blackJack.getDeck();
                msg += `Welcome to Rocket.Chat Black Jack!\n
                Your croupier id is:\n*${deckId}*\n\n`;

                // deal cards
                await blackJack.drawInitialCards();

                const nextPlayer = blackJack.getNextPlayer();

                // get State
                const state = blackJack.getData();

                // keep state in storage
                await Store.persist(persis, room, deckId, state);

                // prompt next user
                msg += blackJack.showStatus();
                msg += `\nNext user: ${nextPlayer}`;

                return msg;
            },

            /**
             * /blackjack hit deckId user
             */
            hit: async (args: Array<string>) => {
                let msg = '';

                // get deck id and user
                const deckId = args[1];
                const currUser = args[2];

                // retrieve state
                const storedState = await Store.findByRoom(persistenceReader, room, deckId);

                // set Croupier data
                const blackJack = new Croupier([], http);
                blackJack.setData(storedState[0]);

                // add card to users pile
                await blackJack.drawOneCardForPlayer(currUser);

                // next player
                const nextPlayer = blackJack.getNextPlayer();

                // update state
                const state = blackJack.getData();
                await Store.persist(persis, room, deckId, state);

                // calculate score
                // display message with updated board
                // prompt next user
                const wmsg = !blackJack.winner ? `\nNext user: ${nextPlayer}` : `\nWinner is: ${blackJack.winner}`;

                msg += blackJack.showStatus();
                msg += wmsg;

                return msg;
            },
            /**
             * /blackjack stay deckId user
             */
            stay: async (args: Array<string>) => {
                let msg = '';

                // get deck id and user
                const deckId = args[1];

                // retrieve state
                const storedState = await Store.findByRoom(persistenceReader, room, deckId);

                // set Croupier data
                const blackJack = new Croupier([], http);
                blackJack.setData(storedState[0]);

                // next player
                const nextPlayer = blackJack.getNextPlayer();

                // update state
                const state = blackJack.getData();
                await Store.persist(persis, room, deckId, state);

                // prompt next user
                msg += blackJack.showStatus();
                msg += `\nNext user: ${nextPlayer}`;

                return msg;
            },
        };

        let message = '';
        if ( methods[action] ) {
            message = await methods[action](contextArgs);
        } else {
            message = 'Input command not recognised, try again.';
        }

        messageStructure
            .setRoom(room)
            .setText(message);

        await modify.getCreator().finish(messageStructure);
    }
}
