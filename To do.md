## Algorithm

1. Make `/blackjack` command
2. If command is not supplied with arguments then jump to **step 5** to set players(Rocket Chat users)
3. Parse and check arguments
    1. If any of the arguments is not a valid player, discard player
    2. If any of the arguments is a player with offline status, discard player
    3. If more than 7 players are found in arguments, discard players from player number 7+
4. Show list of players parsed from command (if any) and min-max players message
    1. List offline players too but strikethrough and with an offline label
5. If more than 2 players with online status are detected, require user action
    1.  Prompt if user wants to start the game with existing players, if so go to step 7
    2. Prompt if user wants to add more players to the game
6. Add more players
    1. Prompt for player input
    2. Check player input is valid and online
    3. Check if max player is not exceeded, then repeat **step 5.1**
    4. If all players are valid and the max limit is met go to next step.
7. Create discussion group with players
8. Start the game
    1. Query https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1 to get a new deck. Make note of the deck_id
    2. Loop through the players twice and for each user:
        1. Draw one card https://deckofcardsapi.com/api/deck/<<deck_id>>/draw/?count=1
        2. Then put the card onto the player’s pile by calling (use player username to designate pile_name) https://deckofcardsapi.com/api/deck/<<deck_id>>/pile/<<pile_name>>/add/?cards=CARD
    3. Display players list with cards dealt in front of each player https://deckofcardsapi.com/api/deck/<<deck_id>>/pile/<<pile_name>>/list/
    4. If any user(s) has got a Black Jack, then call the winner(s), then end game.
    5. Loop through the players once and message each player requiring an action
        1. Stay: Player’s hand is set, display updated list of players with cards, then go to next player.
        2. Hit:  Draw card from deck and add it to the player pile, then display updated list of players with cards, then repeat **step 7.5**, until player chooses to Stay or hand is Bust
    6. Once all players have chosen to Stay then evaluate players hands and announce the winner.
        1. Winner is the player with cards closest to the 21 sum
